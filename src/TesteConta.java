public class TesteConta {
	public static void main(String[] args) {

		ContaCorrente contaC;
		ContaPoupanca contaP;

		// manut, deposit, rend
		contaC = new ContaCorrente(20000, 12345, 0.005, 8, 0.02, " Ciclano",
				"111111111");
		System.out.println("\n\n	CONTA CORRENTE	");
		System.out.println("Numero Conta: " + contaC.getNumeroConta());
		System.out.print("Saldo Inicial da conta: ");
		contaC.printSaldo();
	//	contaC.setTaxaManutencao(0.005);
	//	System.out.print("Saldo Atualizado: ");
	//	contaC.atualizaSaldo();
	//	contaC.printSaldo();
	//	System.out.println("O valor : Atualizado sera diminuido da taxa de manutencao: "+ contaC.Manutencao());
		
		// contaC.setTaxaDeposito(5);
	//	System.out.println("Taxa de deposito: " + contaC.getTaxaDeposito());
	//	contaC.deposita(200);
	//	System.out.println("Saldo apos deposito de 100: " + contaC.getSaldo());
		System.out.print("Saldo Atualizado: ");
		contaC.atualiza(0.5);
		System.out.print("Saldo Atualizado: ");
		contaC.printSaldo();

		
		
		contaP = new ContaPoupanca(10000, 0.02, 12345, "Fulano" , "111111111" );
		System.out.println("	CONTA POUPANCA	");
	//	System.out.println("Cliente: " + "     CPF: " + contaP.getCpf());
		System.out.println("Numero Conta: " + contaP.getNumeroConta());
		System.out.print("Saldo Inicial da conta: ");
		contaP.printSaldo();
	//	System.out.print("Saldo Atualizado com taxa de  " + contaP.getTaxaRendimento()*100 + "%: ");
	//	contaP.atualizaSaldo();
	//	contaP.printSaldo();
	//	contaP.setTaxaDeposito(5);
	//	System.out.println("Taxa de deposito: " + contaP.getTaxaDeposito());
	//	contaP.deposita(100);
	//	System.out.println("Saldo apos deposito de 100: " + contaP.getSaldo());
		System.out.print("Saldo Atualizado: ");
		contaP.atualiza(0.5);
		System.out.print("Saldo Atualizado: ");
		contaP.printSaldo();

	}
}