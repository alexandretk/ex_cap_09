public class ContaCorrente extends Conta {

	private double taxaManutencao;
	private double taxaDeposito;
	private double taxaRendimento;

	public ContaCorrente(double saldo, int numeroConta, double taxaManutencao,
			double taxaDeposito, double taxaRendimento, String nome, String cpf) {
		super(saldo, numeroConta, nome, cpf);
		this.taxaManutencao = taxaManutencao;
		this.taxaDeposito = taxaDeposito;
		this.taxaRendimento = taxaRendimento;
	}

	public void atualizaSaldo() {
		
		this.saldo += this.saldo * taxaRendimento;
	}
	
	public void atualiza(double taxaSelic) {

		this.saldo += this.saldo * taxaSelic;
	}

	public double Manutencao() {

		return this.saldo * this.taxaManutencao;
	}

	public void deposita(double valor) {
		this.saldo += valor - taxaDeposito;
	}

	public double getTaxaRendimento() {
		return taxaRendimento;
	}

	public void setTaxaRendimento(double taxaRendimento) {
		this.taxaRendimento = taxaRendimento;
	}

	public double getTaxaManutencao() {
		return taxaManutencao;
	}

	public void setTaxaManutencao(double taxaManutencao) {
		this.taxaManutencao = taxaRendimento;
	}

	public double getTaxaDeposito() {
		return taxaDeposito;
	}

	public void setTaxaDeposito(double taxaDeposito) {
		this.taxaDeposito = taxaDeposito;
	}

}